# Wireguard Lockdown Mode

If you use a wireguard VPN to proxy all internet traffic, this is a simple firewall to block traffic from leaking outside the tunnel (otherwise known as a kill switch). Works on Linux, tested on Fedora Workstation 38.

Disclaimer: I am not an expert at networking, this was just something I put together because I could not find anything similar at the time. I mainly followed Mullvad VPN for my implementation, looking at the nftables rules that the official Mullvad Linux client uses, and also their document here: https://github.com/mullvad/mullvadvpn-app/blob/main/docs/security.md.

## Install

First make sure your wireguard VPN interface name starts with "wg", and that `FwMark` (firewall mark) is enabled and set to `51820`.

Then:

1. download or clone this repo
2. edit the variables at the top of `wg-lockdown.nft` and then copy the file to /etc/nftables
	- if you don't want to specify all VPN server IPs, read the section below titled "Dynamic VPN Server IPs"
3. copy `wg-lockdown.service` to /etc/systemd/system
4. temporarily enable the firewall using `sudo systemctl start wg-lockdown`
5. make sure everything still works (if not, you can turn it off using `sudo systemctl stop wg-lockdown`, or restart your PC and your firewalls will be back to normal)
6. permanently enable the firewall using `sudo systemctl enable --now wg-lockdown`

## Uninstall

1. run `sudo systemctl disable --now wg-lockdown`

## Warnings and Notes

- we don't fully support ipv6 (like DHCPv6 and NDP), since I am not very familiar with ipv6

- related/established traffic outside the tunnel will be dropped once wg-lockdown is activated, so be careful that you ssh connections might get dropped once you enable the kill switch
	- this is another reason why you should test the firewall using `systemctl start` before calling `systemctl enable`, so that worst case you can force reboot the system to restore it to its original state

- we do not block incoming traffic
	- if you want to do so, I recommend using firewalld or firewall-cmd, which is better at handling connection states than nftables (for example, if you want to block incoming connections from the LAN, but still want to be able to make and establish connections towards the LAN)

- unlike Mullvad, we allow LAN traffic (including all standard local network ranges) by default
	- this is to prevent networking issues when running VMs or rootful containers on your system
		- more info: https://github.com/mullvad/mullvadvpn-app/issues/2320#issuecomment-736766211
	- we use the same local network ranges as Mullvad, and we use a static list for security reasons
		- more info: https://mullvad.net/en/blog/response-to-tunnelcrack-vulnerability-disclosure
	- to disable LAN access, remove all addresses from the `LAN` and `LAN6` variables in `wg-lockdown.nft`

- we allow LAN traffic to go through the tunnel, since this can be useful in some point-to-site or site-to-site configurations
	- though whether or not your OS actually sends LAN traffic through the tunnel, depends on your routing tables and your wireguard VPN's `AllowedIPs` configuration
		- for example, if your `AllowedIPs` is `0.0.0.0` and your home network is 192.168.0.0/24, then *most likely* scenario is that any requests to 192.168.0.0/24 will go out your eth/wifi directly, while requests to other standard local network ranges (like 10.0.0.0/8) will be sent through the tunnel
	- to block LAN traffic from going through the tunnel (but still allow LAN traffic outside of the tunnel), you can change the rule `ip daddr $LAN accept` to `oifname != "wg*" ip daddr $LAN accept` and similarly for LAN6 rules, though I haven't tested this

- we allow forward traffic, which is often used by VMs or rootful containers running on your system

- to add logging for attempted traffic leaks, see the comments about it in `wg-lockdown.nft`

## Dynamic VPN Server IPs

Right now you have to specify all your VPN server IPs at the top of the `wg-lockdown.nft` file. This might be cumbersome if the VPN server IPs are dynamic.

If you don't want to specify a static set of VPN server IPs inside `wg-lockdown.nft`, you can change the rule `ip daddr $VPN_SERVERS meta mark 51820 accept` to just `meta mark 51820 accept`, so that it only checks the firewall mark and not the destination server. This should still work, assuming that your wireguard VPN's `FwMark` is set to `51820`, and no other application on your system is also setting those firewall marks. I'm not sure though, so by default we also check the destination server.

From my research I found that:
- the Mullvad Linux client checks both the firewall mark and the destination server
- if you download wireguard configs from mullvad.net with the "kill switch" option enabled, the included "kill switch" only checks the firewall mark
- the [iVPN's docs on making a custom wireguard kill switch](https://www.ivpn.net/knowledgebase/linux/linux-wireguard-kill-switch/) only checks the firewall mark

## Further Reading

- https://www.ivpn.net/knowledgebase/linux/linux-how-do-i-prevent-vpn-leaks-using-nftables-and-openvpn/
- https://meow464.neocities.org/blog/firewalld-vpn-killswitch
- https://github.com/mullvad/mullvadvpn-app
- https://github.com/mullvad/mullvadvpn-app/blob/main/docs/security.md
- https://mullvad.net/en/blog/closer-look-vpn-vulnerability-cve-2019-14899
- https://mullvad.net/en/blog/response-to-tunnelcrack-vulnerability-disclosure
- https://github.com/corrad1nho/qomui
